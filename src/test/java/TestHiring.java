import org.junit.Assert;
import org.junit.Test;
import testings.Hiring;
import testings.Person;

import java.util.List;

public class TestHiring {

    @Test
    public void testAges() {
        List<Person> persons = new Hiring().hire5PersonWithHobby("Reading");
        persons.forEach(person -> {
            assert person.getAge() >= 18 && person.getAge() <= 80;
        });
    }

    @Test
    public void testHobbyNull () {
        List<Person> personsNullHobby = new Hiring().hire5PersonWithHobby(null);
        Assert.assertEquals(0, personsNullHobby.size());

        List<Person> personsBlankHobby = new Hiring().hire5PersonWithHobby(" ");
        Assert.assertEquals(0, personsBlankHobby.size());
    }

    @Test
    public void testPersonNames () {
        List<Person> persons = new Hiring().hire5PersonWithHobby("Football");
        persons.forEach(person -> {

            System.out.println("Person name: " + person.getName());
            boolean isFirst = true;
            for (char c : person.getName().toCharArray()){
                if (!isFirst) {
                    return;
                }
                assert Character.isUpperCase(c);
                isFirst = false;
            }
        });
    }

    @Test
    public void testPersonHobbies () {
        String hobbyToTest = "Guitar";
        List<Person> persons = new Hiring().hire5PersonWithHobby(hobbyToTest);
        persons.forEach(person -> {
            assert person.getHobby().equals(hobbyToTest);
        });
    }
}
