package testings;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class Person {

    private static final List<String> DEF_NAMES = Arrays.asList("Jeff", "Bob", "Nick", "Monica", "Becky", "Netty");

    private int age;
    private String name, hobby;

    public Person(String hobby) {
        this.hobby = hobby;
        this.name = DEF_NAMES.get(new Random().nextInt(DEF_NAMES.size()));
        this.age = IntStream.rangeClosed(18, 80).findAny().getAsInt();
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }
}
