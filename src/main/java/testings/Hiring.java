package testings;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Hiring {

    public List<Person> hire5PersonWithHobby(String hobby) {
        if (hobby == null || hobby.isEmpty() || hobby.isBlank()) {
            return Collections.emptyList();
        }
       return Stream.generate(() -> new Person(hobby)).limit(5).collect(Collectors.toList());
    }
}
